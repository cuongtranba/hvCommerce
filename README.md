Prerequisite:
- Visual Studio 2015 
- Microsoft ASP.NET and Web Tools 2015 (RC1)
- StyleCop 4.7
- SQL Server

Technologies and frameworks used:
- ASP.NET MVC 6 RC1 on full dotnet framwork 4.5.1
- Autofac 4.0.0 RC1
- Entity framework 6.1.3
- ASP.NET Identity 3.0.0 RC1

How to run on local:
- Create a database in SQL Server
- Update the connection string in appsettings.json in HvCommerce.Web
- Press Controll + F5
- The back-office can access via /Admin using the pre-created account: admin@hvcommerce.com, 1qazZAQ!

How to contribute:
- Suggest features by create new issues or add comments to issues
- Pickup an issue, create your own branch and implement it, then submit a Merge Request when you finished the implemntation and testing
- Remember to fix all the StyleCop violations before submit a Merge Request
