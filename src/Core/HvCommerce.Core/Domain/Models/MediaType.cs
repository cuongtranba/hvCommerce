﻿namespace HvCommerce.Core.Domain.Models
{
    public enum MediaType
    {
        Image,
        Video
    }
}
